import React,{Component} from 'react';
import axios from 'axios'
import jsonpAdapter from 'axios-jsonp'

export default class Search extends Component {

    state = { search: '' }

    componentDidUpdate(prevProps) {

        if (this.props.page !== prevProps.page) {

           
                this.getRecipes(false);
            
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();


        this.getRecipes(true);
    }

    getRecipes = (reset) => {
        const { search } = this.state

        const page = reset===false ?  this.props.page : 1;

        axios({
            url: `http://www.recipepuppy.com/api/?i=onions,garlic&p=${page}&q=${search}`,
            adapter: jsonpAdapter,
        })
        .then(function(response){
            this.props.onResults(reset, response.data.results)
        }.bind(this))
        .catch(function (error) {
            console.log(error)
        });
    }

    render() {

        return (
            <section className="jumbotron text-center">
                <div className="container">
                    <h1 className="jumbotron-heading">Browse Recipes</h1>
                    <form onSubmit={this.handleSubmit}>
                        <div className="input-group">
                            <input 
                                type="text" 
                                name="search"
                                className="form-control" 
                                placeholder="Search by keyword" 
                                required 
                                value={this.state.search}
                                onChange={e => this.setState({search: e.target.value})}
                            />
                            <div className="input-group-append">
                                <button type="submit" className="btn btn-secondary" ><i className="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        );
    }
}

