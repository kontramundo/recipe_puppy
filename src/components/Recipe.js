import React,{Component} from 'react';

export default class Recipe extends Component {

    render() {

        const uuidv1 = require('uuid/v1');
        const { title, ingredients, thumbnail, href } = this.props
        const arrayIngredients = ingredients.split(',');

        const recipeImage = thumbnail ? thumbnail : 'https://via.placeholder.com/286x180.png?text=Puppy+Recipe';

        return (
            <div className="col-md-4">
                <div className="card mb-4 shadow-sm">
                    <img src={recipeImage} className="card-img-top" alt={title} height={180}/>
                    <div className="card-body text-center ">
                        <h5 className="card-title">{title}</h5>
                        <div className="ingredients">
                            {
                                arrayIngredients.map((ingredient, key) => {
                                    return (
                                        <a href="#/" key={uuidv1()} className="text-muted">{ingredient}</a>         
                                    )
                                })
                            }
                        </div>
                        <a href={href} className="btn btn-secondary mt-3"target="_blank" rel="noopener noreferrer"><i className="fas fa-info-circle"></i> More Info</a>
                    </div>
                </div>
            </div>
        );
    }
}