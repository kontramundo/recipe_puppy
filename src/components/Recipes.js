import React,{Component} from 'react';
import Recipe from './Recipe';

export default class Recipes extends Component {



    loadMore = (e) => {
        e.preventDefault();

        const { page } = this.props

        
        this.props.onResults(parseFloat(page)+1)        
    }

    render() {

        const { recipes } = this.props

        return (
            <div className="container">
                <div className="row">
                    <div className="col-12 mb-3">
                        <h5 ><i className="fas fa-utensils"></i> All Recipes</h5>
                        <hr/>
                    </div>
                    {
                        recipes.map((recipe, key) => {
                            return (
                                <Recipe 
                                    key={key}
                                    title={recipe.title}
                                    ingredients={recipe.ingredients}
                                    thumbnail={recipe.thumbnail}
                                    href={recipe.href}
                                />
                            )
                        })
                    }
                    <div className="col-12 text-center mt-2">
                        { recipes.length>0 ? <button type="button" className="btn btn-secondary" onClick={this.loadMore}><i className="fas fa-arrow-down"></i> Load More</button> : '' }
                    </div>
                </div>
            </div>
        );
    }
}