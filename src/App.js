import React,{Component} from 'react';
import Search from './components/Search';
import Recipes from './components/Recipes';

export default class App extends Component {

    state = { 
        reset: false,
        page: 1,
        results: [],
    }

    _handleResults = (reset, results) => {

        const recipes = reset===true ? results : results.concat(this.state.results)

        this.setState({ 
            reset,
            results: recipes
        })
    }

    _handlePage = (page) => {
        
        this.setState({ 
            page
        })
    }

    render() {
        const { reset, page, results } = this.state
        const paginate = reset===true ? 1 : page


        return (
            <React.Fragment>
                <Search onResults={this._handleResults} page={page}/>
                <Recipes onResults={this._handlePage} recipes={results} page={paginate}/>
            </React.Fragment>
        );
    }
}
